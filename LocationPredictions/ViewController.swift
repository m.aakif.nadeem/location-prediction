//
//  ViewController.swift
//  LocationPredictions
//
//  Created by Salman Ahmad on 24/11/2020.
//

import UIKit
import SwiftMessages
import MapKit

struct Places
{
    var Title : String
    var subTitle: String
    var Location: CLLocationCoordinate2D
}

class ViewController: UIViewController, UISearchBarDelegate
{
    @IBOutlet weak var customeMapView: MKMapView!
    @IBOutlet weak var searchBtnUI: UIButton!
    
    var searchResultController: SearchViewController!
    let locationManager = CLLocationManager()
//    var mapView: GMSMapView?
    
    var placesArray : [Places] = []
    var resultsArray: [Places] = []
    
    var swiftyView = MessageView()
    
    let loc1 : Places = Places.init(Title: "Johar Town", subTitle: "Lahore", Location: CLLocationCoordinate2D.init(latitude: 31.4697, longitude: 74.2728))
    let loc2 : Places = Places.init(Title: "Wapda Town", subTitle: "Lahore", Location: CLLocationCoordinate2D.init(latitude: 32.4697, longitude: 74.2728))
    let loc3 : Places = Places.init(Title: "DHA Phase 6", subTitle: "Lahore", Location: CLLocationCoordinate2D.init(latitude: 33.4697, longitude: 74.2728))
    let loc4 : Places = Places.init(Title: "Paragon City", subTitle: "Lahore", Location: CLLocationCoordinate2D.init(latitude: 34.4697, longitude: 74.2728))
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchResultController = SearchViewController()
        searchResultController.delegate = self
        
        placesArray.append(loc1)
        placesArray.append(loc2)
        placesArray.append(loc3)
        placesArray.append(loc4)
    }
    
    @IBAction func searchLocationBtn(_ sender: Any)
    {
        let searchController = UISearchController(searchResultsController: searchResultController)
        searchController.searchBar.delegate = self
        self.present(searchController, animated: true, completion: nil)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        if (searchText != "")
        {
            resultsArray = placesArray.filter { (p) -> Bool in
                if (p.Title.lowercased().contains(searchText.lowercased()))
                {
                    return true
                } else { return false}
            }
    
            self.searchResultController.reloadDataWithArray(self.resultsArray)
        }
        else
        {
            resultsArray.removeAll()
            self.searchResultController.reloadDataWithArray(self.resultsArray)
        }
    }
    
    func showSwiftyAlert(title: String, message: String)
    {
        swiftyView = MessageView.viewFromNib(layout: .messageView)
        swiftyView.configureTheme(.info)
        swiftyView.configureDropShadow()
        swiftyView.configureContent(
            title: nil,
            body: nil,
            iconImage: nil,
            iconText: nil,
            buttonImage: nil,
            buttonTitle: "OK",
            buttonTapHandler: nil)
        swiftyView.configureContent(title: title, body: message)
        swiftyView.button?.addTarget(self, action: #selector(errorMessage), for: .touchUpInside)
        var config = SwiftMessages.Config()
        config.preferredStatusBarStyle = .darkContent
        config.duration = .seconds(seconds: 5)
        SwiftMessages.show(config: config,view: swiftyView)
    }
    
    @objc func errorMessage()
    {
        SwiftMessages.hide(animated: true)
    }
}

extension ViewController: LocateOnTheMap
{
    func locateWithLongitude(_ lon: Double, andLatitude lat: Double, andTitle title: String)
    {
        
        print("Title: \(title)")
        print("Latitude: \(lat)")
        print("Longitude: \(lon)")
        
        self.showSwiftyAlert(title: title, message: "Latitude: \(lat) & Longitude: \(lon)")
        
        let marker = MKPointAnnotation()
        marker.title = title
        marker.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: lon)
        
        let initialLocation = CLLocation(latitude: lat, longitude: lon)
        customeMapView.centerToLocation(initialLocation)
        customeMapView.addAnnotation(marker)
        
    }
}

private extension MKMapView {
  func centerToLocation(
    _ location: CLLocation,
    regionRadius: CLLocationDistance = 1000
  ) {
    let coordinateRegion = MKCoordinateRegion(
      center: location.coordinate,
      latitudinalMeters: regionRadius,
      longitudinalMeters: regionRadius)
    setRegion(coordinateRegion, animated: true)
  }
}
